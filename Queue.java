
import java.util.LinkedList;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Queue extends Application {
	LinkedList<Integer> queue = new LinkedList<Integer>();
	Display display = new Display();
	Button button1 = new Button("Enqueue");
	Button button2 = new Button("Dequeue");
	TextField textField = new TextField();

@Override
public void start(Stage primaryStage) {
	Label lable = new Label("Enter a value: ");
	lable.setAlignment(Pos.BOTTOM_CENTER);
	HBox hBox = new HBox(10);
	hBox.getChildren().addAll(lable, textField, button1, button2);
	hBox.setAlignment(Pos.BOTTOM_CENTER);
	BorderPane borderPane = new BorderPane();
	borderPane.setCenter(display);
	borderPane.setBottom(hBox);

	Scene scene = new Scene(borderPane, 300, 100);
	primaryStage.setScene(scene);
	primaryStage.show();

	textField.setPrefColumnCount(3);
	display.redisplay();

	button1.setOnAction(e -> {
	queue.addLast(Integer.parseInt(textField.getText()));
	display.redisplay();
	});

	button2.setOnAction(e -> {
	if (queue.size() > 0)
	queue.removeFirst();
	display.redisplay();
	});

	}

public class Display extends Pane {
	int x = 30;
	int y = 20;
	int width = 30;
	int height = 20;
	String text = new String("Queue is empty");
	String text2 = new String("Numbers in queue:");

public void redisplay() {
	getChildren().clear();
	if (queue.size() == 0) {
	getChildren().add(new Text(x, y, text));
	} else {
	getChildren().add(new Text(x, y, text2));
	int x2 = 40;
	int y2 = 30;
	for (int i = 0; i < queue.size(); i++) {
	Rectangle rectangle = new Rectangle(x2, y2, width, height);
	rectangle.setStroke(Color.BLACK);
	rectangle.setFill(Color.WHITE);
	getChildren().add(rectangle);
	int x3 = x2 + 5;
	int y3 = y2 + 15;
	String text3 = (" " + queue.get(i) + " ");
	if (queue.size() > 0)
	x2 = x2 + width;
	getChildren().add(new Text(x3, y3, text3));

	}
	}
	}
	}

public static void main(String[] args) {
	launch(args);

	}

}
